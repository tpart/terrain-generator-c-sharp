extends KinematicBody


const GRAVITY = -24.8
const MAX_SPEED = 30
const ACCEL = 5.5
const AIR_ACCEL = 3
const DEACCEL= 16
const AIR_DEACCEL = 2.5
const JUMP_POWER = 15
const MAX_SLOPE_ANGLE = 90
const MOUSE_SENSITIVITY = 0.05
const CONTROLLER_MOUSE_SENSITIVITY_MULTIPLIER = 40

var vel = Vector3.ZERO
var mouse_sensitivity_multiplier = 1

onready var rotation_helper = $RotationHelper
onready var camera = $RotationHelper/Camera


func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)


func _physics_process(delta):
	# Calculate movement vector
	var dir = Vector3.ZERO
	var input_movement_vector = Vector2.ZERO
	if Input.is_action_pressed("move_forward") or Input.is_action_pressed("move_backwards"):
		input_movement_vector.y = Input.get_action_strength("move_forward") - Input.get_action_strength("move_backwards")
	if Input.is_action_pressed("move_right") or Input.is_action_pressed("move_left"):
		input_movement_vector.x =  Input.get_action_strength("move_left") - Input.get_action_strength("move_right")
	input_movement_vector = input_movement_vector.normalized()
	if Input.is_action_pressed("move_forward") or Input.is_action_pressed("move_backwards"):
		dir.z = input_movement_vector.y * Input.get_action_strength("move_forward") - Input.get_action_strength("move_backwards")
	if Input.is_action_pressed("move_right") or Input.is_action_pressed("move_left"):
		dir.x = input_movement_vector.x * Input.get_action_strength("move_left") - Input.get_action_strength("move_right")
	dir = dir.rotated(Vector3.UP, rotation.y)
	
	# Jumping
	if is_on_floor():
		if Input.is_action_just_pressed("jump"):
			vel.y = JUMP_POWER
	
	# Escape menu
	if Input.is_action_just_pressed("ui_cancel"):
		if Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		else:
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	
	# Looking around with controller
	if (Input.is_action_pressed("look_up") or Input.is_action_pressed("look_down") or Input.is_action_pressed("look_left") or Input.is_action_pressed("look_right")):
		rotation_helper.rotate_x(deg2rad((Input.get_action_strength("look_down") - Input.get_action_strength("look_up")) * MOUSE_SENSITIVITY * mouse_sensitivity_multiplier * CONTROLLER_MOUSE_SENSITIVITY_MULTIPLIER))
		self.rotate_y(deg2rad((Input.get_action_strength("look_left") - Input.get_action_strength("look_right")) * MOUSE_SENSITIVITY * mouse_sensitivity_multiplier * CONTROLLER_MOUSE_SENSITIVITY_MULTIPLIER))
		# Clamp rotation
		var camera_rot = rotation_helper.rotation_degrees
		camera_rot.x = clamp(camera_rot.x, -80, 80)
		rotation_helper.rotation_degrees = camera_rot
	
	# Processing movement
	dir.y = 0
	vel.y += delta * GRAVITY
	var hvel = vel
	hvel.y = 0
	var target = dir * MAX_SPEED
	var accel
	if dir.dot(hvel) > 0:
		if is_on_floor():
			accel = ACCEL
		else:
			accel = AIR_ACCEL
	else:
		if is_on_floor():
			accel = DEACCEL
		else:
			accel = AIR_DEACCEL
	hvel = hvel.linear_interpolate(target, accel * delta)
	vel.x = hvel.x
	vel.z = hvel.z
	vel = move_and_slide(vel, Vector3(0, 1, 0), true, 4, deg2rad(MAX_SLOPE_ANGLE), true)


func _input(event):
	if event is InputEventMouseMotion:
		rotation_helper.rotate_x(deg2rad(event.relative.y * MOUSE_SENSITIVITY * mouse_sensitivity_multiplier))
		rotation_helper.rotation_degrees.x = clamp(rotation_helper.rotation_degrees.x, -80, 80)
		self.rotate_y(deg2rad(event.relative.x * MOUSE_SENSITIVITY * mouse_sensitivity_multiplier * -1))
