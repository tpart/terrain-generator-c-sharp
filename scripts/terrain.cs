using Godot;
using System;
using System.Threading;
using System.Collections.Generic;

[Tool]
public class terrain : Spatial
{
	[Export]
	public NodePath Player;

	[Export]
	public int LoadDistance = 6;

	[Export]
	public int ChunkSize = 128;

	[Export]
	public int ChunkResolution = 16;

	[Export]
	public double TerrainHeight = 150;

	public Dictionary<Vector2, StaticBody> Chunks = new Dictionary<Vector2, StaticBody>();
	public List<Vector2> ChunksQueue = new List<Vector2>();
	public KinematicBody PlayerNode;
	public Spatial ChunksNode;
	public OpenSimplexNoise Noise = new OpenSimplexNoise();
	public Material TerrainMaterial = ResourceLoader.Load("res://assets/materials/terrain.tres") as Material;
	public static float UnloadDistanceFactor = 1.5f;


	public override void _Ready()
	{
		ChunksNode = GetNode<Spatial>("ChunksNode");
		Noise.Seed = 0;
		Noise.Octaves = 7;
		Noise.Period = 256;
		Noise.Persistence = 0.5f;

		if (Player != "")
		{
			PlayerNode = GetNode<KinematicBody>(Player);
		}
	}

	public override void _Process(float delta)
	{
		// Add chunks to queue
		Vector3 playerPos = PlayerNode.Translation;
		for (int x = -LoadDistance; x < LoadDistance; x++)
		{
			for (int z = -LoadDistance; z < LoadDistance; z++)
			{
				Vector2 chunkPos = new Vector2(x * ChunkSize + (int)(playerPos.x / ChunkSize) * ChunkSize, z * ChunkSize + (int)(playerPos.z / ChunkSize) * ChunkSize);
				if (!ChunksQueue.Contains(chunkPos) && !Chunks.ContainsKey(chunkPos))
				{
					ChunksQueue.Add(chunkPos);
				}
			}
		}

		// Start generating chunks
		for (int i = 0; i < ChunksQueue.Count; i++)
		{
			Vector2 chunkPos = ChunksQueue[i];
			Chunks[chunkPos] = null;
			ChunksQueue.Remove(chunkPos);
			SurfaceTool surfaceTool = new SurfaceTool();
			ThreadPool.QueueUserWorkItem(
			new WaitCallback(delegate (object state)
			{
				this.GenerateChunk(chunkPos, surfaceTool);
			}));
		}
	}

	public void GenerateChunk(Vector2 chunkPos, SurfaceTool surfaceTool)
	{
		surfaceTool.Begin(Mesh.PrimitiveType.Triangles);
		surfaceTool.AddSmoothGroup(true);
		float squareSize = (float)(ChunkSize / ChunkResolution);
		int squareCount = (int)(ChunkSize / squareSize);
		for (int x = 0; x < squareCount; x++)
		{
			for (int z = 0; z < squareCount; z++)
			{
				surfaceTool.AddVertex(new Vector3(x * squareSize, (float)(Noise.GetNoise2d(x * squareSize + chunkPos.x, z * squareSize + chunkPos.y) * TerrainHeight), z * squareSize));
				surfaceTool.AddVertex(new Vector3((x + 1) * squareSize, (float)(Noise.GetNoise2d((x + 1) * squareSize + chunkPos.x, z * squareSize + chunkPos.y) * TerrainHeight), z * squareSize));
				surfaceTool.AddVertex(new Vector3(x * squareSize, (float)(Noise.GetNoise2d(x * squareSize + chunkPos.x, (z + 1) * squareSize + chunkPos.y) * TerrainHeight), (z + 1) * squareSize));
				surfaceTool.AddVertex(new Vector3(x * squareSize, (float)(Noise.GetNoise2d(x * squareSize + chunkPos.x, (z + 1) * squareSize + chunkPos.y) * TerrainHeight), (z + 1) * squareSize));
				surfaceTool.AddVertex(new Vector3((x + 1) * squareSize, (float)(Noise.GetNoise2d((x + 1) * squareSize + chunkPos.x, z * squareSize + chunkPos.y) * TerrainHeight), z * squareSize));
				surfaceTool.AddVertex(new Vector3((x + 1) * squareSize, (float)(Noise.GetNoise2d((x + 1) * squareSize + chunkPos.x, (z + 1) * squareSize + chunkPos.y) * TerrainHeight), (z + 1) * squareSize));
			}
		}
		surfaceTool.GenerateNormals();
		ArrayMesh mesh = surfaceTool.Commit();
		MeshInstance meshInstance = new MeshInstance();
		meshInstance.Mesh = mesh;
		if (!Engine.EditorHint)
		{
			meshInstance.CreateTrimeshCollision();
		}
		meshInstance.MaterialOverride = TerrainMaterial;
		CallDeferred("AddChunk", meshInstance, chunkPos);
	}

	public void AddChunk(MeshInstance meshInstance, Vector2 chunkPos)
	{
		StaticBody staticBody = new StaticBody();
		staticBody.Name = $"{chunkPos.x},{chunkPos.y}";
		staticBody.Translation = new Vector3(chunkPos.x, 0, chunkPos.y);
		ChunksNode.AddChild(staticBody);
		staticBody.AddChild(meshInstance);
		Chunks[chunkPos] = staticBody;
	}
}
